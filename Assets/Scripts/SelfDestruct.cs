﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    ParticleSystem deathVFX;

    void Start()
    {
        deathVFX = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (deathVFX.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
