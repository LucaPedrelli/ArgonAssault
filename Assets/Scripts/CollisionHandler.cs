﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float reloadTimer = 1f;
    [SerializeField] ParticleSystem crashParticles;

    bool isTransitioning = false;

    // Triggers are collisions with no physics involved, object just pass through each other. Since we have a game on a
    // rail, we don't want our ship to collide with stuff, we just want triggers. Hence, I set Is Trigger to true for
    // every collider on the ship.
    void OnTriggerEnter(Collider other) 
    {
        if (!isTransitioning) { StartCrashSequence(); }
    }

    void StartCrashSequence()
    {
        Invoke("ReloadLevel", reloadTimer); // load next level after reloadTimer seconds
        crashParticles.Play(); // play explosion particles
        GetComponent<MeshRenderer>().enabled = false; // make ship invisible
        GetComponent<PlayerController>().enabled = false; // detach controls
        isTransitioning = true; // so that we don't repeat the previous steps
    }

    void ReloadLevel()
    {
        int currentLvlIdx = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentLvlIdx);
    }
}
