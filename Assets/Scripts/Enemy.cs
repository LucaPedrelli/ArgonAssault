﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    [Tooltip("Particle effect that plays when the enemy dies")]
    [SerializeField] GameObject deathFX; // Collects explosion VFX and SFX. Both components have "play on awake".
    [SerializeField] GameObject hitVFX; // Hit VFX (no SFX). Component has "play on awake"

    [Tooltip("Points this enemy is worth")]
    [SerializeField] int score = 1;
    [Tooltip("Number of hits needed to kill this enemy")]
    [SerializeField] int hitPoints = 1;

    // Not a SerializeField because we don't want to manually drag the scoreBoard on each single enemy instance on map
    ScoreBoard scoreBoard;
    bool exploding = false;
    Transform fxCollectorTransform; // Trasform of the GameObject that we parent our instantiated game effects to


    void Start() 
    {
        // Returns the first ScoreBoard found in the world. Since we only have one, we're safe
        scoreBoard = FindObjectOfType<ScoreBoard>();

        // Since we don't have any collider at instance level for our ships, we need to add a rigidbody at begin play.
        // This allows the instance to take into consideration all of the children collisions
        gameObject.AddComponent<Rigidbody>().useGravity = false;
        fxCollectorTransform = GameObject.FindWithTag("SpawnAtRuntime").transform;
    }

    void OnParticleCollision(GameObject other)
    {
        if (exploding) { return; }
        ProcessHit();
        if (hitPoints == 0)
        {
            Explode();
        }
    }

    void ProcessHit()
    {
        hitPoints--;
        GameObject vfx = Instantiate(hitVFX, transform.position, Quaternion.identity);
        vfx.transform.parent = fxCollectorTransform;
    }

    void Explode()
    {
        scoreBoard.IncreaseScore(score);

        // Instantiate() clones the original object and returns the clone
        // We use Instantiate() instead of childing the particle effect to the gameObject because, otherwise, calling
        // Destroy() on the gameObject would also destroy the particle effect before it finishes.
        // The SelfDestruct script, component of Enemy Explosion prefab, destroys this clone.
        GameObject fx = Instantiate(deathFX, transform.position, Quaternion.identity);
        fx.transform.parent = fxCollectorTransform; // This isn't strictly necessary, it's just for hierarchy tidiness
        exploding = true;
        Destroy(gameObject); // destroying "this" doesn't work, you need to destroy "this.gameObject"
    }
}
