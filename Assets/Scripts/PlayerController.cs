﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; // You must include this from Project Settings/Player/Active Input Handling

public class PlayerController : MonoBehaviour
{
    [Header("Key Bindings")]
    [SerializeField] InputAction movement; // Allows us to set up key bindings from inspector
    [SerializeField] InputAction fire;
    [Header("General movement settings")]
    [Tooltip("How fast ship moves up and down in response to player input")]
    [SerializeField] float speed = 1f;
    [Tooltip("Max distance from rail on the x axis")]
    [SerializeField] float xPositionLimit = 20f;
    [Tooltip("Max distance from rail on the y axis")]
    [SerializeField] float yPositionLimit = 10f;
    [Tooltip("Pitch (x rotation) due to vertical position")]
    [SerializeField] float pitchFactor = -2f;
    [Tooltip("Pitch (x rotation) due to up and down key pressure")]
    [SerializeField] float pitchPressureFactor = -5f;
    [Tooltip("Yaw (y rotation) due to horizontal position")]
    [SerializeField] float yawFactor = 5f;
    [Tooltip("Roll (z rotation) due to left and right key pressure")]
    [SerializeField] float rollPressureFactor = -5f;
    [Header("Components")]
    [SerializeField] GameObject[] lasers;



    void Update()
    {
        float xShift = movement.ReadValue<Vector2>().x;
        float yShift = movement.ReadValue<Vector2>().y;
        ProcessTranslation(xShift, yShift);
        ProcessRotation(xShift, yShift);
        ProcessFiring();
    }

    // OnEnable() is called when this script is enabled by Unity. This occurs when starting the game.
    // Look up Unity's order of execution for event functions online.
    void OnEnable() 
    {
        // Unity's new input system needs to be enabled with this instruction in order to work.
        movement.Enable();
        fire.Enable();
    }

    void OnDisable() {
        movement.Disable();
        fire.Disable();    
    }

    void ProcessTranslation(float xShift, float yShift)
    {
        float xPos = transform.localPosition.x + speed * Time.deltaTime * xShift;
        xPos = Mathf.Clamp(xPos, -xPositionLimit, xPositionLimit);
        float yPos = transform.localPosition.y + speed * Time.deltaTime * yShift;
        yPos = Mathf.Clamp(yPos, -yPositionLimit, yPositionLimit);

        transform.localPosition = new Vector3(xPos, yPos, 0f);
    }

    void ProcessRotation(float xShift, float yShift)
    {
        // pitch = part depending on y pos + part depending on W/S key press
        float pitch = transform.localPosition.y * pitchFactor + yShift * pitchPressureFactor;
        // roll = part depending on A/D key press
        float roll = xShift * rollPressureFactor;
        // yaw = part depending on x pos
        float yaw = transform.localPosition.x * yawFactor;
        
        transform.localRotation = Quaternion.Euler(pitch, roll, yaw);
    }

    void ProcessFiring()
    {
        if (fire.ReadValue<float>() > Mathf.Epsilon)
        {
            foreach (GameObject laser in lasers)
            {
                ActivateLasers(true);
            }
        }
        else
        {
            foreach (GameObject laser in lasers)
            {
                ActivateLasers(false);
            }
        }
    }

    void ActivateLasers(bool activate)
    {
        foreach (GameObject laser in lasers)
        {
            // This way we stop emitting new laser beams but the one we already emitted keep existing
            ParticleSystem.EmissionModule em = laser.GetComponent<ParticleSystem>().emission;
            em.enabled = activate;
        }
    }
}
