﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    // Awake() is called at initialization
    void Awake() 
    {
        if (FindObjectsOfType<MusicPlayer>().Length > 1)
        {
            // Destroy yourself if there is more than one music player (singleton)
            Destroy(gameObject);
        }
        else
        {
            // So that when the player dies the music doesn't restart
            DontDestroyOnLoad(gameObject);
        }
    }
}
