# Argon Assault :space_invader:

Game with a camera on rails. The player controls a ship and shoots at incoming enemies!

This game was made following this [C# Unity Game Developer 3D course](https://www.udemy.com/course/unitycourse2/). I focused on learning how to implement gameplay features rather than level design.

## Notes on assets

My horrible internet connection doesn't allow me to upload assets in a decent amount of time. If you clone this repo and open it, Unity will throw a bunch of error at you saying that the terrain requires a series of assets and it is not being able to find them.  
To solve this, open the package manager and import [Star Sparrow Modular Spaceship](https://assetstore.unity.com/packages/3d/vehicles/space/star-sparrow-modular-spaceship-73167), [Terrain Tools Sample Asset Pack](https://assetstore.unity.com/packages/2d/textures-materials/nature/terrain-tools-sample-asset-pack-145808), [Standard Assets for Unity 2018.4](https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2018-4-32351), and [Skybox Vol 2](https://assetstore.unity.com/packages/2d/textures-materials/sky/skybox-volume-2-nebula-3392).